# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
{
    'name': 'Ticket Management',
    'name_de_DE': 'Ticket Verwaltung',
    'version': '2.2.0',
    'author': 'virtual things, Openlabs, Andres Vargas',
    'email':'info@virtual-things.biz',
    'website': 'http://www.tryton.org/',
    'description': 'Ticket management module.',
    'description_de_DE': '''Ticketverwaltung
    ''',
    'depends' : [
        'ir',
        'company',
        'company_work_time',
        'timesheet',
        'project'
        ],
    'xml' : [
        'ticket.xml',
        'work.xml',
        'configuration.xml',
        ],
    'translation': [
        'locale/de_DE.po',
        ],
}
