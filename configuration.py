from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval


class Configuration(ModelSingleton, ModelView, ModelSQL):
    "Ticket Configuration"
    _name = "ticket.configuration"
    _description = __doc__

    ticket_sequence = fields.Property(
        fields.Many2One('ir.sequence', 'Sequence Ticket',
            domain = [
                ('company', 'in', [Eval('company',False)]),
                ('code', '=', 'ticket.work')
            ], depends=['company'], required = True
        ))

Configuration()
