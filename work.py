# -*- coding: UTF-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
'Ticket Management'
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Bool, Eval, In, Or
from trytond.transaction import Transaction
from trytond.pool import Pool


_STATES = {
        'readonly': Or(
            Bool(In(Eval('state'), ['closed'])),
            Not(Bool(Eval('active'))))}
_DEPENDS = ['state', 'active']


class Ticket(ModelSQL, ModelView):
    'Ticket'
    _name = 'ticket.work'
    _description = __doc__
    _inherits = {'timesheet.work': 'work'}

    work = fields.Many2One('timesheet.work', 'Work', required=True,
            ondelete='CASCADE', states=_STATES, depends=_DEPENDS, select=1)
    code = fields.Char('Code', readonly=True, select=1)
    requestor = fields.Many2One('party.party', 'Requestor',
            states={
                'required': Bool(In(Eval('state'), ['closed'])),
                'readonly': Or(
                Bool(In(Eval('state'), ['closed'])),
                Not(Bool(Eval('active'))))
            }, depends=['state', 'active'])
    comment = fields.Text('Comment', states=_STATES, depends=_DEPENDS)
    assigned_to = fields.Many2One('company.employee', 'Assigned To',
            states=_STATES, depends=_DEPENDS, select=1)
    state = fields.Selection([
            ('open', 'Open'),
            ('closed', 'Closed'),], 'State')

    def default_state(self):
        return 'open'

    def default_active(self):
        return True

    def default_company(self):
        return Transaction().context.get('company') or False

    def set_code(self, ticket_id):
        '''Set the code

        :param ticket_id: ID of the ticket
        :return: True if success
        '''
        sequence_obj = Pool().get('ir.sequence')
        config_obj = Pool().get('ticket.configuration')

        ticket = self.browse(ticket_id)
        if ticket.code:
            return True

        config = config_obj.browse(0)
        code = sequence_obj.get_id(config.ticket_sequence.id)
        self.write(ticket_id, {
                'code': code,
        })
        return True

    def create(self, values):
        '''
        Create the record and assign a code

        :param values: Values for new record
        :return: ID of the created record
        '''
        ticket_id = super(Ticket, self).create(values)
        self.set_code(ticket_id)
        return ticket_id

    def delete(self, ids):
        timesheet_work_obj = Pool().get('timesheet.work')

        if isinstance(ids, (int, long)):
            ids = [ids]

        # Get the timesheet works linked to the ticket works
        ticket_works = self.browse(ids)
        timesheet_work_ids = [tw.work.id for tw in ticket_works]

        res = super(Ticket, self).delete(ids)

        timesheet_work_obj.delete(timesheet_work_ids)
        return res

Ticket()


